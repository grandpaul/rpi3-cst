#!/bin/sh
#

# Copyright (c) 2018 Linaro
# Author: Ying-Chun Liu (PaulLiu) <paul.liu@linaro.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This program is used to split armstub8.bin to BL1.bin and FIP.bin
# Ex: ./split_rpi3_bl1_fip.sh --bl1 bl1.bin --fip fip.bin armstub8.bin


if [ $# -le 0 ]; then
    echo "Usage: $0 [--bl1 <bl1.pad.bin>] [--fip <fip.bin>] [--bl1size 131072] <armstub8.bin>"
    exit 0
fi

ARMSTUB8="$1"
BL1SIZE=131072

while [ $# -gt 0 ]; do
    key="$1"

    case "$key" in
	--bl1)
	    BL1FILE="$2"
	    shift
	    shift
	    ;;
	--fip)
	    FIPFILE="$2"
	    shift
	    shift
	    ;;
	--bl1size)
	    BL1SIZE="$2"
	    shift
	    shift
	    ;;
	*)
	    ARMSTUB8="$key"
 	    shift
	    ;;
    esac
done

if [ x"$BL1FILE" != x ]; then
    dd if="$ARMSTUB8" of="$BL1FILE" bs=1 count="$BL1SIZE"
fi
if [ x"$FIPFILE" != x ]; then
    dd if="$ARMSTUB8" of="$FIPFILE" bs=1 skip="$BL1SIZE"
fi
