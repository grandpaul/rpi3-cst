rpi3 signing tools
===============================

We provide several tools for code signing.


## split_rpi3_bl1_fip.sh

This tool is used to split armstub8.bin. Because armstub8.bin is combine
with BL1 and FIP, before modifying it, we need to split it.

Example:
```
./split_rpi3_bl1_fip.sh --bl1 bl1.bin --fip fip.bin armstub8.bin
```

And you'll get bl1.bin and fip.bin. To combine them back, just cat.


## update_bl3x.sh

This tool is used to update BL32 (OPTEE) or BL33 (U-boot) in armstub8.bin.
You need to have the ROT key that signed armstub8.bin before.
If you don't have the original ROT key,
you shouldn't use this program. It will destroy your armstub8.bin.

Before you run, please make sure you have split_rpi3_bl1_fip.sh, fiptool, and
cert_create in your PATH.

Example:
```
env PATH=$PATH:`pwd`:../arm-trusted-firmware/tools/fiptool:../arm-trusted-firmware/tools/cert_create \
    ./update_bl3x.sh --rot-key /tmp/rot_key.pem \
    		     --armstub8 /media/user/boot/armstub8.bin \
		     --out /tmp/armstub8.bin \
		     --tos-fw tee-header_v2.bin \
		     --tos-fw-extra1 tee-pager_v2.bin \
		     --tos-fw-extra2 tee-pageable_v2.bin
```

The above command updates OPTEE in your old armstub8.bin.
The old armstub8.bin is at /media/user/boot/ in the above example.
And the output is /tmp/armstub8.bin. Later you can copy armstub8.bin back
to your SDCard and boot.

## hack_bl1_rothash.py

Note: This tool is very dangerous. Don't use it if you really know what you
are doing.

This tool is basically used to dump the public key hash of the ROT key inside
BL1 or BL2. And even able to replace it. So that means we can actually change
the ROT key if we want. This is because RPi3 doesn't have a real boot ROM or
FUSE to protect the key hash. Since BL1/BL2 is just a binary on SDCard,
we can always replace the key hash.

Example: To output the keyhash in BL1.
```
./hack_bl1_rothash.py --bl1 bl1.bin  --rotpk_sha256_output=rotpk_sha256.bin
```

Example: To update the keyhash in BL1
```
./hack_bl1_rothash.py --bl1 bl1.bin  \
		      --rotpk_sha256_output=old_rotpk_sha256.bin \
		      --rotpk_sha256_replace=new_rotpk_sha256.bin
```


Example: To replace ROT key in armstub8.bin
```
TMPDIR001=`mktemp -d`

# first, we generate our own ROT_KEY
openssl genrsa 2048 > $TMPDIR001/rot_key.pem

# second, we generate the public key hash
openssl rsa -in $TMPDIR001/rot_key.pem -pubout -outform DER \
| openssl dgst -sha256 -binary > $TMPDIR001/rotpk_sha256.bin

# split the armstub8.bin
./split_rpi3_bl1_fip.sh --bl1 $TMPDIR001/bl1.bin \
			--fip $TMPDIR001/fip.bin \
			armstub8.bin

# unpack the fip in order to get BL2
fiptool unpack --out $TMPDIR001 $TMPDIR001/fip.bin

# replace the key hash in BL1
./hack_bl1_rothash.py --bl1 $TMPDIR001/bl1.bin \
		      --rotpk_sha256_replace=$TMPDIR001/rotpk_sha256.bin

# replace the key hash in BL2
./hack_bl1_rothash.py --bl1 $TMPDIR001/tb-fw.bin \
		      --rotpk_sha256_replace=$TMPDIR001/rotpk_sha256.bin

# resign the rest of the stuff and create new armstub8.bin
# (Please make sure your PATH has the tools that update_bl3x.sh needed)
./update_bl3x.sh --rot-key $TMPDIR001/rot_key.pem \
		 --armstub8 armstub8.bin \
		 --new-bl1 $TMPDIR001/bl1.bin \
		 --tb-fw $TMPDIR001/tb-fw.bin \
		 --out armstub8_new.bin

```

