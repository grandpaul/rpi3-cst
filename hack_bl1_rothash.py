#!/usr/bin/env python3
#

# Copyright (c) 2018 Linaro
# Author: Ying-Chun Liu (PaulLiu) <paul.liu@linaro.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import getopt

# for KMP algorithm please juse see Wikipedia.

def kmp_table(pattern):
    pos=1
    cnd=0
    W = pattern
    T = [0] * (len(W)+1)
    T[0] = -1

    while (pos < len(W)):
        if (W[pos] == W[cnd]):
            T[pos] = T[cnd]
            pos = pos + 1
            cnd = cnd + 1
        else:
            T[pos] = cnd
            cnd = T[cnd]
            while (cnd >= 0 and W[pos] != W[cnd]):
                cnd = T[cnd]
            pos = pos + 1
            cnd = cnd + 1
    T[pos] = cnd
    return T

def kmp(filename, pattern):
    W = pattern
    P = []
    nP = 0
    j = 0
    k = 0
    T = kmp_table(W)
    f = open(filename, "rb")
    try:
        byte = f.read(1)
        while (byte != b''):
            if (W[k] == byte):
                j = j+1
                byte = f.read(1)
                k = k+1
                if (k == len(W)):
                    P.append(j-k)
                    nP = nP+1
                    k = T[k]
            else:
                k=T[k]
                if (k<0):
                    j=j+1
                    byte = f.read(1)
                    k=k+1
                
    finally:
        f.close()
    return P

if __name__ == '__main__':

    # Handle argc
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["bl1=",
                                                      "rotpk_sha256_output=",
                                                      "rotpk_sha256_replace="
                                                     ]
        )
    except getopt.GetoptError as err:
        # print help information and exit:
        print (str(err)) # will print something like "option -a not recognized"
        sys.exit(2)
    rotpk_sha256_output = None
    rotpk_sha256_replace = None
    bl1 = None
    for o, a in opts:
        if o in ("--bl1"):
            bl1 = a
        elif o in ("--rotpk_sha256_output"):
            rotpk_sha256_output = a
        elif o in ("--rotpk_sha256_replace"):
            rotpk_sha256_replace = a
        else:
            assert False, "unhandled option"
    if (not bl1):
        print("Please assign --bl1")
        sys.exit(2)

    # DER header
    derHeader = [b'\x30', b'\x31', b'\x30', b'\x0D',
                 b'\x06', b'\x09', b'\x60', b'\x86',
                 b'\x48', b'\x01', b'\x65', b'\x03',
                 b'\x04', b'\x02', b'\x01', b'\x05',
                 b'\x00', b'\x04', b'\x20']

    # Search for DER header
    places = kmp(bl1, derHeader);
    print("Found following DER headers:")
    print(places)

    if (len(places) != 1):
        print ("Not able to do any operations because len(places) != 1")
        sys.exit(2)

    # Read KeyHash
    f=open(bl1, "rb")
    f.seek(places[0],0)
    derHeader1=f.read(len(derHeader))
    keyHash = f.read(256 // 8)
    f.close()

    print("ROT_PK_SHA256:")
    for i in range(len(keyHash)):
        print('%02x'%(keyHash[i]), end='')
        if (i%16==15):
            print('')
        else:
            print(' ', end='')

    # Output keyHash
    if (rotpk_sha256_output):
        f=open(rotpk_sha256_output, "wb")
        f.write(keyHash)
        f.close()

    # Replace keyHash
    if (rotpk_sha256_replace):
        # Read new keyHash to memory
        f=open(rotpk_sha256_replace, "rb")
        newKeyHash = f.read(256 // 8)
        f.close()

        # Update keyHash to the file
        f=open(bl1, "r+b")
        f.seek(places[0] + len(derHeader),0)
        f.write(newKeyHash)
        f.close()
