#!/bin/sh
#

# Copyright (c) 2018 Linaro
# Author: Ying-Chun Liu (PaulLiu) <paul.liu@linaro.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Usage
if [ $# -le 0 ]; then
    echo "Usage: $0 --armstub8 <armstub8.bin> --rot-key rot.pem [--tos-fw tee-header_v2.bin] [--tos-fw-extra1 tee-pager_v2.bin] [--tos-fw-extra2 tee-pageable_v2.bin] [--nt-fw u-boot.bin]"
    exit 0
fi


# Create a temp directory so that we can play all things in there

TMPDIR=`mktemp -d`

# Check Tools

SPLITTOOL=split_rpi3_bl1_fip.sh
FIPTOOL=fiptool
CERT_CREATE=cert_create
command "$SPLITTOOL" > "$TMPDIR"/test_splittool.txt 2>&1
command "$FIPTOOL" > "$TMPDIR"/test_fiptool.txt 2>&1
command "$CERT_CREATE" > "$TMPDIR"/test_cert_create.txt 2>&1


# Handle parameters
ROT_KEY=""
TOSFW=""
TOSFWEXTRA1=""
TOSFWEXTRA2=""
NTFW=""
OUTFILE=""
TBFW=""
NEWBL1=""

while [ $# -gt 0 ]; do
    key="$1"

    case "$key" in
        --rot-key)
            ROT_KEY="$2"
            shift
            shift
            ;;
        --armstub8)
            ARMSTUBFILE="$2"
            shift
            shift
            ;;
	--tos-fw)
	    TOSFW="$2"
	    shift
	    shift
	    ;;
	--tos-fw-extra1)
	    TOSFWEXTRA1="$2"
	    shift
	    shift
	    ;;
	--tos-fw-extra2)
	    TOSFWEXTRA2="$2"
	    shift
	    shift
	    ;;
	--nt-fw)
	    NTFW="$2"
	    shift
	    shift
	    ;;
	--tb-fw)
	    TBFW="$2"
	    shift
	    shift
	    ;;
	--new-bl1)
	    NEWBL1="$2"
	    shift
	    shift
	    ;;
	--out)
	    OUTFILE="$2"
	    shift
	    shift
	    ;;
        *)
            shift
            ;;
    esac
done

# * You should generate your ROT key first.
#   - Use "openssl genrsa 2048 > rot.pem"
# * Your ATF should build with ROT_KEY= rot.pem file. So that
#   BL1 can have the hash value of the public key of ROT_KEY.
#   - Or, use "openssl rsa -in rot.pem -pubout -outform DER \
#	       | openssl dgst -sha256 -binary > rotpk_sha256.bin"
#     And assign ROTPK_HASH=rotpk_sha256.bin
# * BL1 will use this public key to verify all other keys.
# * If you don't assign ROT_KEY or ROTPK_HASH, during the build process of
#   ATF, it will generate a rot.pem for you and put into the build directory.
#   You should keep it otherwise you will not be able to update your FIP
#   manually by this script.
# * BL1 suppose to be a ROM so that once the hash value is burn to the ROM,
#   who holds ROT_KEY (private key) who owns the priviledge.
#   So ROT key must be the same in BL1 and in this script.
#   Otherwise you will break your FIP because it won't pass.
#

if [ x"$ROT_KEY" = x ]; then
    echo "Please assign --rot-key"
    exit 0
fi

mkdir -p "$TMPDIR"/keys
cp -f "$ROT_KEY" "$TMPDIR"/keys/rot.pem

# Now lets deal the armstub8.bin. It has BL1 and FIP. So let's split it first.

if [ x"$ARMSTUBFILE" = x ]; then
    echo "Please assign --armstub8"
    exit 0
fi

mkdir -p "$TMPDIR"/armstub
cp -f "$ARMSTUBFILE" "$TMPDIR"/armstub/armstub8.bin
"$SPLITTOOL" --fip "$TMPDIR"/armstub/fip.bin \
	     --bl1 "$TMPDIR"/armstub/bl1.pad.bin \
	     "$TMPDIR"/armstub/armstub8.bin

# We can extract the contents of the fip now.


rm -rf "$TMPDIR"/fip
mkdir -p "$TMPDIR"/fip

"$FIPTOOL" info "$TMPDIR"/armstub/fip.bin
"$FIPTOOL" unpack --out "$TMPDIR"/fip "$TMPDIR"/armstub/fip.bin

# Update some contents here.
if [ x"$TOSFW" != x ]; then
    if [ -e "$TOSFW" ]; then
	cp -f "$TOSFW" "$TMPDIR"/fip/tos-fw.bin
    fi
fi
if [ x"$TOSFWEXTRA1" != x ]; then
    if [ -e "$TOSFWEXTRA1" ]; then
	cp -f "$TOSFWEXTRA1" "$TMPDIR"/fip/tos-fw-extra1.bin
    fi
fi
if [ x"$TOSFWEXTRA2" != x ]; then
    if [ -e "$TOSFWEXTRA2" ]; then
	cp -f "$TOSFWEXTRA2" "$TMPDIR"/fip/tos-fw-extra2.bin
    fi
fi
if [ x"$NTFW" != x ]; then
    if [ -e "$NTFW" ]; then
	cp -f "$NTFW" "$TMPDIR"/fip/nt-fw.bin
    fi
fi
if [ x"$TBFW" != x ]; then
    if [ -e "$TBFW" ]; then
	cp -f "$TBFW" "$TMPDIR"/fip/tb-fw.bin
    fi
fi

# Since ROT_KEY verify other keys. Actually other keys can be a one-time key.
# That means we can ignore those keys in the original FIP. And generate all
# those keys by ourselves and replace it. Using the ROT key we don't need to
# worry about that.

rm -f "$TMPDIR"/fip/*-cert.bin
"$CERT_CREATE" -n --rot-key "$TMPDIR"/keys/rot.pem \
	       --tfw-nvctr 0 \
	       --ntfw-nvctr 0 \
	       --tb-fw "$TMPDIR"/fip/tb-fw.bin \
	       --soc-fw "$TMPDIR"/fip/soc-fw.bin \
	       --tos-fw "$TMPDIR"/fip/tos-fw.bin \
	       --tos-fw-extra1 "$TMPDIR"/fip/tos-fw-extra1.bin \
	       --tos-fw-extra2 "$TMPDIR"/fip/tos-fw-extra2.bin \
	       --nt-fw "$TMPDIR"/fip/nt-fw.bin \
	       --trusted-key-cert "$TMPDIR"/fip/trusted-key-cert.bin \
	       --soc-fw-key-cert "$TMPDIR"/fip/soc-fw-key-cert.bin \
	       --tos-fw-key-cert "$TMPDIR"/fip/tos-fw-key-cert.bin \
	       --nt-fw-key-cert "$TMPDIR"/fip/nt-fw-key-cert.bin \
	       --tb-fw-cert "$TMPDIR"/fip/tb-fw-cert.bin \
	       --soc-fw-cert "$TMPDIR"/fip/soc-fw-cert.bin \
	       --tos-fw-cert "$TMPDIR"/fip/tos-fw-cert.bin \
	       --nt-fw-cert "$TMPDIR"/fip/nt-fw-cert.bin

# repack
"$FIPTOOL" create "$TMPDIR"/fip_new.bin \
    --tb-fw "$TMPDIR"/fip/tb-fw.bin \
    --soc-fw "$TMPDIR"/fip/soc-fw.bin \
    --tos-fw "$TMPDIR"/fip/tos-fw.bin \
    --tos-fw-extra1 "$TMPDIR"/fip/tos-fw-extra1.bin \
    --tos-fw-extra2 "$TMPDIR"/fip/tos-fw-extra2.bin \
    --nt-fw "$TMPDIR"/fip/nt-fw.bin \
    --trusted-key-cert "$TMPDIR"/fip/trusted-key-cert.bin \
    --soc-fw-key-cert "$TMPDIR"/fip/soc-fw-key-cert.bin \
    --tos-fw-key-cert "$TMPDIR"/fip/tos-fw-key-cert.bin \
    --nt-fw-key-cert "$TMPDIR"/fip/nt-fw-key-cert.bin \
    --tb-fw-cert "$TMPDIR"/fip/tb-fw-cert.bin \
    --soc-fw-cert "$TMPDIR"/fip/soc-fw-cert.bin \
    --tos-fw-cert "$TMPDIR"/fip/tos-fw-cert.bin \
    --nt-fw-cert "$TMPDIR"/fip/nt-fw-cert.bin

if [ x"$NEWBL1" = x ]; then
    NEWBL1="$TMPDIR"/armstub/bl1.pad.bin
fi

cat "$NEWBL1" \
    "$TMPDIR"/fip_new.bin > "$TMPDIR"/armstub8.bin
if [ x"$OUTFILE" != x ]; then
    cp -f "$TMPDIR"/armstub8.bin "$OUTFILE"
fi
